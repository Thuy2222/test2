<?php
namespace Step\Acceptance;

use Page\Acceptance\RegisterPage;

class RegisterStep extends \AcceptanceTester
{
    public function register($name, $email, $phone, $password, $passwordconfirm)
    {
        $registerPage = new RegisterPage($this);
        $registerPage->onPage()
            ->fillName($name)
            ->fillEmail($email)
            ->fillPhone($phone)
            ->fillPassword($password)
            ->fillPasswordconfirm($passwordconfirm);
    }
}